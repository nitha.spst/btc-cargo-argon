import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";

// Ngx Bootstrap
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ToastrModule } from "ngx-toastr";
import { TagInputModule } from "ngx-chips";
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";

 

// Angular Social-x-login
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angularx-social-login";




import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";
import { PresentationModule } from "./pages/presentation/presentation.module";

import { BrowserModule } from '@angular/platform-browser';
import { ComponentsModule } from "./components/components.module";

import { AppRoutingModule } from './app-routing.module';
import { LineLoginComponent } from './custom/components/social-login/line-login/line-login.component';
import { GoogleLoginComponent } from './custom/components/social-login/google-login/google-login.component';
import { FacebookLoginComponent } from './custom/components/social-login/facebook-login/facebook-login.component';
import { PurchaseComponent } from './custom/pages/purchase/purchase.component';
import { CartComponent } from './custom/pages/cart/cart.component';
import { OrderdetailComponent } from './custom/pages/orderdetail/orderdetail.component';
import { TrackingComponent } from './custom/pages/tracking/tracking.component';
import { LoginComponent } from './custom/pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    LineLoginComponent,
    GoogleLoginComponent,
    FacebookLoginComponent,
    PurchaseComponent,
    CartComponent,
    OrderdetailComponent,
    TrackingComponent,
    LoginComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ComponentsModule,
    BsDropdownModule.forRoot(),
    AppRoutingModule,
    ToastrModule.forRoot(),
    CollapseModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TagInputModule,
    PresentationModule,
    BrowserModule,
    AppRoutingModule,

    // Angular Social-x-login module
    SocialLoginModule,
  ],
  providers: [
    // Angular Social-x-login provider
    {
      provide: "SocialAuthServiceConfig",
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              "234737017700-tua2d197nk46bshjaq68ocqn04l3i1lf.apps.googleusercontent.com"
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider("1093195391498445"),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
