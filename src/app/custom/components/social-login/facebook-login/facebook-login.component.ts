import { Component, OnInit } from "@angular/core";
import {
  SocialAuthService,
  FacebookLoginProvider,
} from "angularx-social-login";

@Component({
  selector: "app-facebook-login",
  templateUrl: "./facebook-login.component.html",
  styleUrls: ["./facebook-login.component.scss"],
})
export class FacebookLoginComponent implements OnInit {
  constructor(private authService: SocialAuthService) {}

  ngOnInit(): void {}

  signInWithFacebook(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((res) => {
      console.log(res);
    });
  }
}
