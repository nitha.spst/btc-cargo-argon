import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth-layout/auth-layout.component";
import { PresentationComponent } from "./pages/presentation/presentation.component";
import { DashboardComponent } from "./pages/dashboards/dashboard/dashboard.component";
import { PurchaseComponent } from "./custom/pages/purchase/purchase.component";
import { CartComponent } from "./custom/pages/cart/cart.component";
import { OrderdetailComponent } from "./custom/pages/orderdetail/orderdetail.component";
import { TrackingComponent } from "./custom/pages/tracking/tracking.component";
import { LoginComponent } from "./custom/pages/login/login.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  {
    path: "admin",
    component: AdminLayoutComponent,
    children: [
      { path: "purchase", component: PurchaseComponent },
      { path: "cart", component: CartComponent },
      { path: "orderdetail", component: OrderdetailComponent },
      { path: "tracking", component: TrackingComponent },
      { path: "", redirectTo: "purchase", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      //useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
